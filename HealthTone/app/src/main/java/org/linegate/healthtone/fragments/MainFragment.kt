package org.linegate.healthtone.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import org.linegate.healthtone.MainPagerAdapter
import org.linegate.healthtone.R
import org.linegate.healthtone.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_main, container, false)
        _binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainPagerAdapter = MainPagerAdapter(this)
        mainPagerAdapter.addFragment(OverviewFragment())
        mainPagerAdapter.addFragment(SpecificStatisticsFragment(SpecificStatisticsFragment.Type.Sleep))
        mainPagerAdapter.addFragment(SpecificStatisticsFragment(SpecificStatisticsFragment.Type.Eat))
        mainPagerAdapter.addFragment(SpecificStatisticsFragment(SpecificStatisticsFragment.Type.Water))
        mainPagerAdapter.addFragment(SpecificStatisticsFragment(SpecificStatisticsFragment.Type.Steps))

        binding.pager.adapter = mainPagerAdapter
        binding.pager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position != 0) {
                    binding.upFloatingButton.visibility = View.VISIBLE
                } else {
                    binding.upFloatingButton.visibility = View.GONE
                }
            }
        })
        binding.upFloatingButton.setOnClickListener {
            binding.pager.setCurrentItem(0, true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}