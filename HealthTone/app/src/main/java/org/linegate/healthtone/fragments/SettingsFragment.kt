package org.linegate.healthtone.fragments

import android.os.Bundle
import android.view.*
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import org.linegate.healthtone.appviewmodel.AppViewModel
import org.linegate.healthtone.HumanIndex
import org.linegate.healthtone.NotFirstNullInputFilter
import org.linegate.healthtone.R
import org.linegate.healthtone.databinding.FragmentSettingsBinding

class SettingsFragment : Fragment() {

    private var _binding: FragmentSettingsBinding? = null
    val binding
        get() = _binding!!

    private val appViewModel: AppViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_main, container, false)
        _binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_settings, container, false)
        binding.appViewModel = appViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //binding.testChart.show(listOf("test" to 1.5F, "test2" to 1.5F))

        val notFirstNullFilter = NotFirstNullInputFilter()
        binding.ageInput.filters = arrayOf(notFirstNullFilter)
        binding.heightInput.filters = arrayOf(notFirstNullFilter)
        binding.weightInput.filters = arrayOf(notFirstNullFilter)
        binding.ageInput.addTextChangedListener { calculateNormalValues() }
        binding.heightInput.addTextChangedListener { calculateNormalValues() }
        binding.weightInput.addTextChangedListener { calculateNormalValues() }

        binding.saveButton.setOnClickListener {
            appViewModel.setCurrentSettings(getAgeInput(), getHeightInput(), getWeightInput())
        }
    }

    private fun calculateNormalValues() {
        val age = getAgeInput()
        if (age != null) {
            val height = getHeightInput()
            val weight = getWeightInput()

            binding.heightInputLayout.isHelperTextEnabled = true
            binding.heightInputLayout.helperText = getString(R.string.normal_value_d_s, HumanIndex.getNormalHeight(age, weight), getString(R.string.cm))

            binding.weightInputLayout.isHelperTextEnabled = true
            binding.weightInputLayout.helperText = getString(R.string.normal_value_d_s, HumanIndex.getNormalWeight(age, height), getString(R.string.kg))
        } else {
            binding.heightInputLayout.isHelperTextEnabled = false
            binding.weightInputLayout.isHelperTextEnabled = false
        }
    }

    private fun getAgeInput(): Int? {
        return if (binding.ageInput.text.isNullOrEmpty()) null else binding.ageInput.text.toString().toInt()
    }

    private fun getHeightInput(): Int? {
        return if (binding.heightInput.text.isNullOrEmpty()) null else binding.heightInput.text.toString().toInt()
    }

    private fun getWeightInput(): Int? {
        return if (binding.weightInput.text.isNullOrEmpty()) null else binding.weightInput.text.toString().toInt()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}