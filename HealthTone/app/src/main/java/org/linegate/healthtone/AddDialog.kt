package org.linegate.healthtone

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.linegate.healthtone.appviewmodel.AppViewModel

class AddDialog(context: Context, val type: Type, val appViewModel: AppViewModel) {

    enum class Type { WATER, SLEEP, EAT }

    private val dialogBuilder = AlertDialog.Builder(context)

    private val viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_add, null, false)

    private lateinit var dialog: AlertDialog

    private lateinit var editTextInput: TextInputEditText

    init {
        dialogBuilder.setTitle(context.getString(R.string.add))
        dialogBuilder.setView(viewInflated)
        val editTextLayout: TextInputLayout = viewInflated.findViewById(R.id.dialogInputLayout)
        editTextLayout.suffixText = when (type) {
            Type.WATER -> context.getString(R.string.ml)
            Type.EAT -> context.getString(R.string.cal)
            Type.SLEEP -> context.getString(R.string.hours)
        }

        editTextInput = viewInflated.findViewById(R.id.dialogInput)
        editTextInput.filters = arrayOf(NotFirstNullInputFilter())
        editTextInput.addTextChangedListener {
            if (!it.isNullOrEmpty()) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = true
            } else {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            }
        }

        dialogBuilder.setPositiveButton(android.R.string.ok, null)

        dialogBuilder.setNegativeButton(android.R.string.cancel, null)
    }

    fun show() {
        dialog = dialogBuilder.show()
        val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveButton.isEnabled = false
        positiveButton.setOnClickListener {
            val intValue = editTextInput.text.toString().toInt()
            when (type) {
                Type.WATER -> appViewModel.addTodayWater(intValue)
                Type.EAT -> appViewModel.addTodayEat(intValue)
                Type.SLEEP -> appViewModel.addTodaySleep(intValue)
            }
            dialog.dismiss()
        }
    }
}