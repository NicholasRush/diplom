package org.linegate.healthtone

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import org.linegate.healthtone.appviewmodel.AppViewModel
import org.linegate.healthtone.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private val appViewModel: AppViewModel by viewModels()

    private lateinit var stepsService: StepsService

    private lateinit var binding: ActivityMainBinding

    private lateinit var navController: NavController

    private val dayChangedBroadcastReceiver = object : BroadcastReceiver() {
        val intentFiler = IntentFilter().apply {
            addAction(Intent.ACTION_TIME_TICK)
            addAction(Intent.ACTION_TIMEZONE_CHANGED)
            addAction(Intent.ACTION_TIME_CHANGED)
            addAction(Intent.ACTION_DATE_CHANGED)
        }

        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == Intent.ACTION_TIME_TICK || intent?.action == Intent.ACTION_TIMEZONE_CHANGED || intent?.action == Intent.ACTION_TIME_CHANGED) {
                appViewModel.onTimeUpdate()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration.Builder(R.id.mainFragment).build()
        setupActionBarWithNavController(navController, appBarConfiguration)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), 0)
        }

        val stepsServiceIntent = Intent(applicationContext, StepsService::class.java)

        //startService(stepsServiceIntent)
        if (!StepsService.isRunning) {
            stepsServiceIntent.action = Constants.actionStartForegroundService
            startForegroundService(stepsServiceIntent)
        }

        bindService(stepsServiceIntent, object: ServiceConnection {
            override fun onServiceConnected(className: ComponentName, binder: IBinder) {
                stepsService = (binder as StepsService.StepsServiceBinder).getService()
                stepsService.appViewModel = appViewModel
            }

            override fun onServiceDisconnected(name: ComponentName?) {

            }
        }, BIND_AUTO_CREATE)

        registerReceiver(dayChangedBroadcastReceiver, dayChangedBroadcastReceiver.intentFiler)
    }

    override fun onDestroy() {
        unregisterReceiver(dayChangedBroadcastReceiver)
        stepsService.createViewModel()
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}