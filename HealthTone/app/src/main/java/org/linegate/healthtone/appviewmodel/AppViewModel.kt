package org.linegate.healthtone.appviewmodel

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.linegate.healthtone.HumanIndex
import org.linegate.healthtone.R
import java.io.File
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.lang.Integer.max
import kotlin.math.abs
import kotlin.math.min
import kotlin.math.roundToInt

class AppViewModel(application: Application) : AndroidViewModel(application) {

    private fun requireApplication(): Application {
        return getApplication<Application>()
    }

    private val resources = requireApplication().resources

    private fun getColor(@ColorRes colorRes: Int): Int {
        return ResourcesCompat.getColor(resources, colorRes, null)
    }

    companion object {

        val defaultAge = 20

        val todayDayFormat = SimpleDateFormat("yyyy.MM.dd")
    }

    data class DayInWeek (var day: Calendar, var water: Int = 0, var eat: Int = 0, var sleep: Int = 0, var steps: Int = 0)

    //Always 6
    private val previousWeekDays = mutableListOf<DayInWeek>()

    //<editor-fold desc="Preferences">

    private val sharedPreferences = requireApplication().applicationContext.getSharedPreferences("preferences", Context.MODE_PRIVATE)

    fun saveToPreferences(key: String, value: Int?) {
        if (value == null) {
            sharedPreferences.edit().remove(key).apply()
        } else {
            sharedPreferences.edit().putInt(key, value).apply()
        }
    }

    fun saveToPreferences(key: String, value: String?) {
        if (value == null) {
            sharedPreferences.edit().remove(key).apply()
        } else {
            sharedPreferences.edit().putString(key, value).apply()
        }
    }

    fun getIntFromPreferences(key: String): Int? {
        return if (sharedPreferences.contains(key)) sharedPreferences.getInt(key, 0) else null
    }

    fun getStringFromPreferences(key: String): String? {
        return if (sharedPreferences.contains(key)) sharedPreferences.getString(key, "") else null
    }

    //</editor-fold>

    //<editor-fold desc="Overview">

    private val _overviewStatusText = MutableLiveData<String>()
    val overviewStatusText: LiveData<String> = _overviewStatusText

    private val _overviewStatusColor = MutableLiveData<@receiver:ColorInt Int>()
    val overviewStatusColor: LiveData<Int> = _overviewStatusColor

    private val _overviewStatusIcon = MutableLiveData<Drawable>()
    val overviewStatusIcon: LiveData<Drawable> = _overviewStatusIcon

    private val _overviewStatusDetailText = MutableLiveData<String>()
    val overviewStatusDetailText: LiveData<String> = _overviewStatusDetailText

    private val _overviewBars = MutableLiveData<List<Pair<String, Float>>>()
    val overviewBars: LiveData<List<Pair<String, Float>>> = _overviewBars

    private val _overviewBarsColors = MutableLiveData<List<Int>>()
    val overviewBarsColors: LiveData<List<Int>> = _overviewBarsColors

    private fun updateOverviewStatistics() {
        val diffs = mutableListOf<Float>()
        for (i in 0..5) {
            diffs.add(getAbsoluteDailyStatisticsDiff(
                getAbsoluteStatisticsDiff(previousWeekDays[i].water, getDailyWaterNorm()),
                getAbsoluteStatisticsDiff(previousWeekDays[i].eat, getDailyEatNorm()),
                getAbsoluteStatisticsDiff(previousWeekDays[i].sleep, getDailySleepNorm()),
                getAbsoluteStatisticsDiff(previousWeekDays[i].steps, getDailyStepsNorm())
            ))
        }

        _overviewBars.value = listOf(
            "-6" to diffs[5],
            "-5" to diffs[4],
            "-4" to diffs[3],
            "-3" to diffs[2],
            "-2" to diffs[1],
            "-1" to diffs[0],
            resources.getString(R.string.norm) to 0.1f
        )

        val barColors = mutableListOf<Int>(getColor(R.color.green))
        for (i in 0..5) {
            barColors.add(0, blendGoodToBadColors(min(diffs[i], 1f)))
        }
        _overviewBarsColors.value = barColors

        var sumLast4 = 0f
        for (i in 2..5) {
            sumLast4 += diffs[i]
        }

        var sumPrev2 = 0f
        for (i in 0..1) {
            sumPrev2 += diffs[i]
        }

        val overallAvg = (sumPrev2 + sumLast4) / 6
        if (overallAvg < 0.1f) {
            _overviewStatusIcon.value = AppCompatResources.getDrawable(requireApplication().applicationContext, R.drawable.ic_baseline_sentiment_very_satisfied_24)
            _overviewStatusText.value = resources.getString(R.string.very_good)
        } else if (overallAvg < 0.2f) {
            _overviewStatusIcon.value = AppCompatResources.getDrawable(requireApplication().applicationContext, R.drawable.ic_baseline_sentiment_satisfied_alt_24)
            _overviewStatusText.value = resources.getString(R.string.good)
        } else if (overallAvg < 0.3f) {
            _overviewStatusIcon.value = AppCompatResources.getDrawable(requireApplication().applicationContext, R.drawable.ic_baseline_sentiment_satisfied_24)
            _overviewStatusText.value = resources.getString(R.string.normal)
        } else if (overallAvg < 0.4f) {
            _overviewStatusIcon.value = AppCompatResources.getDrawable(requireApplication().applicationContext, R.drawable.ic_baseline_sentiment_dissatisfied_24)
            _overviewStatusText.value = resources.getString(R.string.not_good)
        } else {
            _overviewStatusIcon.value = AppCompatResources.getDrawable(requireApplication().applicationContext, R.drawable.ic_baseline_sentiment_very_dissatisfied_24)
            _overviewStatusText.value = resources.getString(R.string.bad)
        }

        _overviewStatusColor.value = blendGoodToBadColors(min(overallAvg, 1f))

        val diffAvgPrev2Last4 = sumLast4 / 4 - sumPrev2 / 2
        if (diffAvgPrev2Last4 > 0.1f) {
            _overviewStatusDetailText.value = resources.getString(R.string.statistics_improves)
        } else if (diffAvgPrev2Last4 < -0.1f) {
            _overviewStatusDetailText.value = resources.getString(R.string.statistics_worsens)
        } else {
            _overviewStatusDetailText.value = resources.getString(R.string.statistics_not_changing)
        }
    }

    //</editor-fold>

    //<editor-fold desc="Sleep daily">

    private val _sleepDailyStatusColor = MutableLiveData<@receiver:ColorInt Int>()
    val sleepDailyStatusColor: LiveData<Int> = _sleepDailyStatusColor

    private val _sleepDailyBarBackgroundColor = MutableLiveData<@receiver:ColorInt Int>()
    val sleepDailyBarBackgroundColor: LiveData<Int> = _sleepDailyBarBackgroundColor

    private val _sleepDailyBarColor = MutableLiveData<@receiver:ColorInt Int>()
    val sleepDailyBarColor: LiveData<Int> = _sleepDailyBarColor

    private val _sleepDailyBar = MutableLiveData<Pair<Float, Float>>()
    val sleepDailyBar: LiveData<Pair<Float, Float>> = _sleepDailyBar
    
    private val _sleepDailyStatistics = MutableLiveData<String>()
    val sleepDailyStatistics: LiveData<String> = _sleepDailyStatistics

    private val _sleepDailyBars = MutableLiveData<List<Pair<String, Float>>>()
    val sleepDailyBars: LiveData<List<Pair<String, Float>>> = _sleepDailyBars

    private val _sleepDailyBarsColors = MutableLiveData<List<Int>>()
    val sleepDailyBarsColors: LiveData<List<Int>> = _sleepDailyBarsColors

    private fun updateSleepDailyStatistics() {
        val diffs = mutableListOf<Float>()
        for (i in 0..5) {
            diffs.add(getAbsoluteStatisticsDiff(previousWeekDays[i].sleep, getDailySleepNorm()))
        }

        _sleepDailyBars.value = listOf(
            "-6" to diffs[5],
            "-5" to diffs[4],
            "-4" to diffs[3],
            "-3" to diffs[2],
            "-2" to diffs[1],
            "-1" to diffs[0],
            resources.getString(R.string.norm) to 0.1f
        )

        val barColors = mutableListOf<Int>(getColor(R.color.green))
        for (i in 0..5) {
            barColors.add(0, blendGoodToBadColors(min(diffs[i], 1f)))
        }
        _sleepDailyBarsColors.value = barColors
    }

    //</editor-fold>

    //<editor-fold desc="Eat daily">
    
    private val _eatDailyStatusColor = MutableLiveData<@receiver:ColorInt Int>()
    val eatDailyStatusColor: LiveData<Int> = _eatDailyStatusColor

    private val _eatDailyBarBackgroundColor = MutableLiveData<@receiver:ColorInt Int>()
    val eatDailyBarBackgroundColor: LiveData<Int> = _eatDailyBarBackgroundColor

    private val _eatDailyBarColor = MutableLiveData<@receiver:ColorInt Int>()
    val eatDailyBarColor: LiveData<Int> = _eatDailyBarColor

    private val _eatDailyBar = MutableLiveData<Pair<Float, Float>>()
    val eatDailyBar: LiveData<Pair<Float, Float>> = _eatDailyBar

    private val _eatDailyStatistics = MutableLiveData<String>()
    val eatDailyStatistics: LiveData<String> = _eatDailyStatistics

    private val _eatDailyBars = MutableLiveData<List<Pair<String, Float>>>()
    val eatDailyBars: LiveData<List<Pair<String, Float>>> = _eatDailyBars

    private val _eatDailyBarsColors = MutableLiveData<List<Int>>()
    val eatDailyBarsColors: LiveData<List<Int>> = _eatDailyBarsColors

    private fun updateEatDailyStatistics() {
        val diffs = mutableListOf<Float>()
        for (i in 0..5) {
            diffs.add(getAbsoluteStatisticsDiff(previousWeekDays[i].eat, getDailyEatNorm()))
        }

        _eatDailyBars.value = listOf(
            "-6" to diffs[5],
            "-5" to diffs[4],
            "-4" to diffs[3],
            "-3" to diffs[2],
            "-2" to diffs[1],
            "-1" to diffs[0],
            resources.getString(R.string.norm) to 0.1f
        )

        val barColors = mutableListOf<Int>(getColor(R.color.green))
        for (i in 0..5) {
            barColors.add(0, blendGoodToBadColors(min(diffs[i], 1f)))
        }
        _eatDailyBarsColors.value = barColors
    }

    //</editor-fold>

    //<editor-fold desc="Water daily">

    private val _waterDailyStatusColor = MutableLiveData<@receiver:ColorInt Int>()
    val waterDailyStatusColor: LiveData<Int> = _waterDailyStatusColor

    private val _waterDailyBarBackgroundColor = MutableLiveData<@receiver:ColorInt Int>()
    val waterDailyBarBackgroundColor: LiveData<Int> = _waterDailyBarBackgroundColor

    private val _waterDailyBarColor = MutableLiveData<@receiver:ColorInt Int>()
    val waterDailyBarColor: LiveData<Int> = _waterDailyBarColor

    private val _waterDailyBar = MutableLiveData<Pair<Float, Float>>()
    val waterDailyBar: LiveData<Pair<Float, Float>> = _waterDailyBar

    private val _waterDailyStatistics = MutableLiveData<String>()
    val waterDailyStatistics: LiveData<String> = _waterDailyStatistics

    private val _waterDailyBars = MutableLiveData<List<Pair<String, Float>>>()
    val waterDailyBars: LiveData<List<Pair<String, Float>>> = _waterDailyBars

    private val _waterDailyBarsColors = MutableLiveData<List<Int>>()
    val waterDailyBarsColors: LiveData<List<Int>> = _waterDailyBarsColors

    private fun updateWaterDailyStatistics() {
        val diffs = mutableListOf<Float>()
        for (i in 0..5) {
            diffs.add(getAbsoluteStatisticsDiff(previousWeekDays[i].water, getDailyWaterNorm()))
        }

        _waterDailyBars.value = listOf(
            "-6" to diffs[5],
            "-5" to diffs[4],
            "-4" to diffs[3],
            "-3" to diffs[2],
            "-2" to diffs[1],
            "-1" to diffs[0],
            resources.getString(R.string.norm) to 0.1f
        )

        val barColors = mutableListOf<Int>(getColor(R.color.green))
        for (i in 0..5) {
            barColors.add(0, blendGoodToBadColors(min(diffs[i], 1f)))
        }
        _waterDailyBarsColors.value = barColors
    }

    //</editor-fold>

    //<editor-fold desc="Steps daily">

    private val _stepsDailyStatusColor = MutableLiveData<@receiver:ColorInt Int>()
    val stepsDailyStatusColor: LiveData<Int> = _stepsDailyStatusColor

    private val _stepsDailyBarBackgroundColor = MutableLiveData<@receiver:ColorInt Int>()
    val stepsDailyBarBackgroundColor: LiveData<Int> = _stepsDailyBarBackgroundColor

    private val _stepsDailyBarColor = MutableLiveData<@receiver:ColorInt Int>()
    val stepsDailyBarColor: LiveData<Int> = _stepsDailyBarColor

    private val _stepsDailyBar = MutableLiveData<Pair<Float, Float>>()
    val stepsDailyBar: LiveData<Pair<Float, Float>> = _stepsDailyBar

    private val _stepsDailyStatistics = MutableLiveData<String>()
    val stepsDailyStatistics: LiveData<String> = _stepsDailyStatistics

    private val _stepsDailyBars = MutableLiveData<List<Pair<String, Float>>>()
    val stepsDailyBars: LiveData<List<Pair<String, Float>>> = _stepsDailyBars

    private val _stepsDailyBarsColors = MutableLiveData<List<Int>>()
    val stepsDailyBarsColors: LiveData<List<Int>> = _stepsDailyBarsColors

    private fun updateStepsDailyStatistics() {
        val diffs = mutableListOf<Float>()
        val absoluteDiffs = mutableListOf<Float>()
        for (i in 0..5) {
            diffs.add(getStatisticsDiff(previousWeekDays[i].steps, getDailyStepsNorm()))
            absoluteDiffs.add(getAbsoluteStatisticsDiff(diffs[i]))
        }

        _stepsDailyBars.value = listOf(
            "-6" to diffs[5],
            "-5" to diffs[4],
            "-4" to diffs[3],
            "-3" to diffs[2],
            "-2" to diffs[1],
            "-1" to diffs[0],
            resources.getString(R.string.norm) to 1f
        )

        val barColors = mutableListOf<Int>(getColor(R.color.green))
        for (i in 0..5) {
            if (diffs[i] > 1) {
                barColors.add(0, getColor(R.color.green))
            } else {
                barColors.add(0, blendGoodToBadColors(min(absoluteDiffs[i], 1f)))
            }
        }
        _stepsDailyBarsColors.value = barColors
    }

    //</editor-fold>

    //<editor-fold desc="Current settings">

    val currentAge: Int?
        get() = getIntFromPreferences("age")

    fun getCurrentAgeOrDefault(): Int {
        return currentAge?: defaultAge
    }

    val currentHeight: Int?
        get() = getIntFromPreferences("height")

    fun getCurrentHeightOrDefault(): Int {
        return currentHeight?: HumanIndex.getNormalHeight(getCurrentAgeOrDefault(), currentWeight)
    }

    val currentWeight: Int?
        get() = getIntFromPreferences("weight")

    fun getCurrentWeightOrDefault(): Int {
        return currentWeight?: HumanIndex.getNormalWeight(getCurrentAgeOrDefault(), currentHeight)
    }

    fun setCurrentSettings(age: Int?, height: Int?, weight: Int?) {
        with (sharedPreferences.edit()) {
            if (age == null) {
                remove("age")
            } else {
                putInt("age", age)
            }

            if (height == null) {
                remove("height")
            } else {
                putInt("height", height)
            }

            if (weight == null) {
                remove("weight")
            } else {
                putInt("weight", weight)
            }

            apply()
        }

        updateDailySteps()
        updateDailySleep()
        updateDailyEat()
        updateDailyWater()
        updateWeekStatistics()
    }

    //</editor-fold>

    //<editor-fold desc="Daily">

    fun getDailyWaterNorm(): Int {
        return HumanIndex.getDailyWaterNorm(getCurrentWeightOrDefault(), getBMIDiff())
    }

    fun getDailyStepsNorm(): Int {
        return HumanIndex.getDailyStepsNorm()
    }

    fun getDailySleepNorm(): Int {
        return HumanIndex.getDailySleepNorm(getCurrentAgeOrDefault())
    }

    fun getDailyEatNorm(): Int {
        return HumanIndex.getDailyEatNorm(
            getCurrentAgeOrDefault(),
            getCurrentHeightOrDefault(),
            getCurrentWeightOrDefault(),
            HumanIndex.getActivityCoefficientBySteps(todaySteps)
        )
    }

    //</editor-fold>

    //<editor-fold desc="Today">

    private var todayDay: Calendar
        get() {
            val calendar = Calendar.getInstance()
            calendar.time = todayDayFormat.parse(getStringFromPreferences("todayDay"))
            return calendar
        }
        set(value) { saveToPreferences("todayDay", todayDayFormat.format(value)) }

    private var todayWater: Int
        get() = getIntFromPreferences("todayWater")?: 0
        set(value) {
            saveToPreferences("todayWater", value)
            updateDailyWater()
        }

    fun addTodayWater(value: Int) {
        todayWater += value
    }
    
    private fun updateDailyWater() {
        val norm = getDailyWaterNorm()
        _waterDailyStatistics.value = resources.getString(R.string.d_slash_d_s, todayWater, norm, resources.getString(R.string.ml))
        val diff = getStatisticsDiff(todayWater, norm)
        if (diff > 1) {
            _waterDailyBarColor.value = getColor(R.color.green)
            _waterDailyBarBackgroundColor.value = getColor(R.color.red)
            _waterDailyBar.value = Pair(1 / diff, 1f)
        } else {
            _waterDailyBarColor.value = getColor(R.color.green)
            _waterDailyBarBackgroundColor.value = getColor(R.color.green_white)
            _waterDailyBar.value = Pair(diff, 1f)
        }
        _waterDailyStatusColor.value = blendGoodToBadColors(min(1f, getAbsoluteStatisticsDiff(diff)))
    }

    private var todayEat: Int
        get() = getIntFromPreferences("todayEat")?: 0
        set(value) {
            saveToPreferences("todayEat", value)
            updateDailyEat()
        }

    fun addTodayEat(value: Int) {
        todayEat += value
    }

    private fun updateDailyEat() {
        val norm = getDailyEatNorm()
        _eatDailyStatistics.value = resources.getString(R.string.d_slash_d_s, todayEat, norm, resources.getString(R.string.cal))
        val diff = getStatisticsDiff(todayEat, norm)
        if (diff > 1) {
            _eatDailyBarColor.value = getColor(R.color.green)
            _eatDailyBarBackgroundColor.value = getColor(R.color.red)
            _eatDailyBar.value = Pair(1 / diff, 1f)
        } else {
            _eatDailyBar.value = Pair(diff, 1f)
            _eatDailyBarColor.value = getColor(R.color.green)
            _eatDailyBarBackgroundColor.value = getColor(R.color.green_white)
        }
        _eatDailyStatusColor.value = blendGoodToBadColors(min(1f, getAbsoluteStatisticsDiff(diff)))
    }

    private var todaySleep: Int
        get() = getIntFromPreferences("todaySleep")?: 0
        set(value) {
            saveToPreferences("todaySleep", value)
            updateDailySleep()
        }

    fun addTodaySleep(value: Int) {
        val calendar = Calendar.getInstance()
        val thisDayMax = calendar.get(Calendar.HOUR_OF_DAY) - todaySleep
        todaySleep += min(value, thisDayMax)
        var hoursDiff = value - thisDayMax
        if (hoursDiff > 0) {
            for (i in 0..5) {
                val iDayHoursDiff = min(24 - previousWeekDays[i].sleep, hoursDiff)
                previousWeekDays[i].sleep += iDayHoursDiff
                hoursDiff -= iDayHoursDiff
                if (hoursDiff == 0) {
                    break
                }
            }
            savePreviousWeek()
        }
    }

    private fun updateDailySleep() {
        val norm = getDailySleepNorm()
        _sleepDailyStatistics.value = resources.getString(R.string.d_slash_d_s, todaySleep, norm, resources.getString(R.string.hours))
        val diff = getStatisticsDiff(todaySleep, norm)
        if (diff > 1) {
            _sleepDailyBarColor.value = getColor(R.color.green)
            _sleepDailyBarBackgroundColor.value = getColor(R.color.red)
            _sleepDailyBar.value = Pair(1 / diff, 1f)
        } else {
            _sleepDailyBar.value = Pair(diff, 1f)
            _sleepDailyBarColor.value = getColor(R.color.green)
            _sleepDailyBarBackgroundColor.value = getColor(R.color.green_white)
        }
        _sleepDailyStatusColor.value = blendGoodToBadColors(min(1f, getAbsoluteStatisticsDiff(diff)))
    }

    private var todaySteps: Int
        get() = getIntFromPreferences("todaySteps")?: 0
        set(value) {
            saveToPreferences("todaySteps", value)
            updateDailySteps()
        }

    fun addTodaySteps(value: Int) {
        todaySteps += value
    }

    private fun updateDailySteps() {
        val norm = getDailyStepsNorm()
        _stepsDailyStatistics.value = resources.getString(R.string.d_slash_d_s, todaySteps, norm, resources.getString(R.string.steps))
        val diff = getStatisticsDiff(todaySteps, norm)
        if (diff > 1) {
            _stepsDailyBar.value = Pair(1 / diff, 1f)
            _stepsDailyBarColor.value = getColor(R.color.green_white)
            _stepsDailyBarBackgroundColor.value = getColor(R.color.green)
            _stepsDailyStatusColor.value = getColor(R.color.green)
        } else {
            _stepsDailyBar.value = Pair(diff, 1f)
            _stepsDailyBarColor.value = getColor(R.color.green)
            _stepsDailyBarBackgroundColor.value = getColor(R.color.green_white)
            _stepsDailyStatusColor.value = blendGoodToBadColors(getAbsoluteStatisticsDiff(diff))
        }
    }

    //</editor-fold>

    private fun getStatisticsDiff(current: Int, base: Int): Float {
        return current.toFloat() / base
    }

    private fun getAbsoluteStatisticsDiff(diff: Float): Float {
        return abs(1 - diff)
    }

    private fun getAbsoluteStatisticsDiff(current: Int, base: Int): Float {
        return abs(1 - current.toFloat() / base)
    }

    private fun blendGoodToBadColors(blendCoefficient: Float): Int {
        return ColorUtils.blendARGB(ResourcesCompat.getColor(resources, R.color.green, null), ResourcesCompat.getColor(resources, R.color.red, null), blendCoefficient)
    }

    private fun getAbsoluteDailyStatisticsDiff(absoluteWaterDiff: Float, absoluteEatDiff: Float, absoluteSleepDiff: Float, absoluteStepsDiff: Float): Float {
        if (absoluteStepsDiff <= 1) {
            return (absoluteWaterDiff + absoluteEatDiff + absoluteSleepDiff + absoluteStepsDiff) / 4
        } else {
            return (absoluteWaterDiff + absoluteEatDiff + absoluteSleepDiff) / 3
        }
    }

    fun getBMI(): Float {
        return if (currentHeight != null && currentWeight != null) {
            HumanIndex.getBMI(currentHeight!!, currentWeight!!)
        } else HumanIndex.getNormalBMI(getCurrentAgeOrDefault())
    }

    fun getBMIDiff(): Float {
        return HumanIndex.getBMIDiff(getBMI(), HumanIndex.getNormalBMI(getCurrentAgeOrDefault()))
    }

    fun getDaysDiff(base: Calendar, new: Calendar): Int {
        val newCopy = Calendar.getInstance()
        newCopy.time = new.time
        var diff = 0
        while (newCopy.get(Calendar.YEAR) < base.get(Calendar.YEAR) || newCopy.get(Calendar.MONTH) < base.get(Calendar.MONTH) || newCopy.get(Calendar.DAY_OF_MONTH) < base.get(Calendar.DAY_OF_MONTH)) {
            diff--
            newCopy.add(Calendar.DAY_OF_MONTH, 1)
        }
        while (newCopy.get(Calendar.YEAR) > base.get(Calendar.YEAR) || newCopy.get(Calendar.MONTH) > base.get(Calendar.MONTH) || newCopy.get(Calendar.DAY_OF_MONTH) > base.get(Calendar.DAY_OF_MONTH)) {
            diff++
            newCopy.add(Calendar.DAY_OF_MONTH, -1)
        }
        return diff
    }

    fun onTimeUpdate() {
        val calendar = Calendar.getInstance()
        val daysDiff = getDaysDiff(todayDay, calendar)
        if (daysDiff != 0) {
            if (daysDiff > 0) {
                val daysDiffRestricted = min(daysDiff, 6)
                for (i in daysDiffRestricted..1) {
                    val calendar = Calendar.getInstance()
                    calendar.add(Calendar.DAY_OF_MONTH, -i)
                    previousWeekDays.add(0, DayInWeek(calendar))
                    previousWeekDays.removeLast()
                }
                if (daysDiff < 7) {
                    previousWeekDays[daysDiff - 1].eat = todayEat
                    previousWeekDays[daysDiff - 1].water = todayWater
                    previousWeekDays[daysDiff - 1].sleep = todaySleep
                    previousWeekDays[daysDiff - 1].steps = todaySteps
                }
                todaySleep = 0
                todayWater = 0
                todayEat = 0
                todaySteps = 0
            } else {
                if (daysDiff > -7) {
                    todayEat = previousWeekDays[-daysDiff - 1].eat
                    todayWater = previousWeekDays[-daysDiff - 1].water
                    todaySleep = previousWeekDays[-daysDiff - 1].sleep
                    todaySteps = previousWeekDays[-daysDiff - 1].steps
                } else {
                    todaySleep = 0
                    todayWater = 0
                    todayEat = 0
                    todaySteps = 0
                }
                val daysDiffRestricted = max(daysDiff, -6)
                for (i in daysDiffRestricted..-1) {
                    val calendar = Calendar.getInstance()
                    calendar.add(Calendar.DAY_OF_MONTH, -(7 + i))
                    previousWeekDays.add(previousWeekDays.size, DayInWeek(calendar))
                    previousWeekDays.removeFirst()
                }
            }

            todayDay = calendar

            savePreviousWeek()

            updateWeekStatistics()
        }
    }

    private fun savePreviousWeek() {
        File(requireApplication().applicationContext.filesDir, "week").delete()
        val weekFileOutputStream = requireApplication().applicationContext.openFileOutput("week", Context.MODE_PRIVATE)
        val weekWriter = OutputStreamWriter(weekFileOutputStream)
        for (i in 0..5) {
            weekWriter.appendLine(todayDayFormat.format(previousWeekDays[i].day))
            weekWriter.appendLine(previousWeekDays[i].sleep.toString())
            weekWriter.appendLine(previousWeekDays[i].water.toString())
            weekWriter.appendLine(previousWeekDays[i].eat.toString())
            weekWriter.appendLine(previousWeekDays[i].steps.toString())
        }
        weekWriter.close()
        weekFileOutputStream.close()
    }

    fun updateWeekStatistics() {
        updateOverviewStatistics()
        updateEatDailyStatistics()
        updateWaterDailyStatistics()
        updateSleepDailyStatistics()
        updateStepsDailyStatistics()
    }

    init {
        if (getStringFromPreferences("todayDay") == null) {
            todayDay = Calendar.getInstance()
        }

        for (i in 1..6) {
            val calendar = todayDay
            calendar.add(Calendar.DAY_OF_MONTH, -i)
            previousWeekDays.add(DayInWeek(calendar))
        }

        val weekFile = File(requireApplication().applicationContext.filesDir, "week")
        if (weekFile.exists()) {
            val weekFileInputStream =  requireApplication().applicationContext.openFileInput("week")
            val weekReader = InputStreamReader(weekFileInputStream)
            val weekLines = weekReader.readLines()
            weekReader.close()
            weekFileInputStream.close()
            for (i in 0..5) {
                val calendar = Calendar.getInstance()
                calendar.time = todayDayFormat.parse(weekLines[i*5])
                previousWeekDays[i].day = calendar
                previousWeekDays[i].sleep = weekLines[i*5 + 1].toInt()
                previousWeekDays[i].water = weekLines[i*5 + 2].toInt()
                previousWeekDays[i].eat = weekLines[i*5 + 3].toInt()
                previousWeekDays[i].steps = weekLines[i*5 + 4].toInt()
            }
        }

        onTimeUpdate()

        updateDailyWater()
        updateDailyEat()
        updateDailySleep()
        updateDailySteps()
        updateWeekStatistics()
    }

}