package org.linegate.healthtone.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.db.williamchart.data.*
import org.linegate.healthtone.AddDialog
import org.linegate.healthtone.appviewmodel.AppViewModel
import org.linegate.healthtone.R
import org.linegate.healthtone.databinding.FragmentOverviewBinding

class OverviewFragment : Fragment() {

    private var _binding: FragmentOverviewBinding? = null
    val binding
        get() = _binding!!

    private val appViewModel: AppViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_main, container, false)
        _binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_overview, container, false)
        binding.appViewModel = appViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //binding.sleepBar.matrix.setValues(floatArrayOf(0f))
        //binding.sleepBar.scale = Scale(0f, 100f)
        //binding.sleepBar.setAnimation()
        /*binding.sleepBar.scale = Scale(0f, 1f)
        binding.sleepBarBackground.scale = Scale(0f, 1f)
        binding.sleepBar.barsColor = resources.getColor(R.color.green)
        binding.sleepBarBackground.show(listOf("" to 0f, "" to 1f))
        binding.sleepBar.animate(listOf("" to 0f, "" to 0.5f))
        binding.sleepBar2.show(listOf("" to 0f, "" to 1f))
        binding.sleepBar3.show(listOf("" to 0f, "" to 1f))
        binding.sleepBar4.show(listOf("" to 0f, "" to 1f))*/
        //binding.sleepBar.animate(listOf("sleep" to 1f, "test" to 0f, "really" to 1f, "pain" to 0.2f))
        //binding.sleepBar.drawBars(listOf(Frame(0f, 0f, 10f, 10f)))

        binding.sleepBarAdd.setOnClickListener { AddDialog(requireContext(), AddDialog.Type.SLEEP, appViewModel).show() }
        binding.eatBarAdd.setOnClickListener { AddDialog(requireContext(), AddDialog.Type.EAT, appViewModel).show() }
        binding.waterBarAdd.setOnClickListener { AddDialog(requireContext(), AddDialog.Type.WATER, appViewModel).show() }

        binding.overviewDetailsButton.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToOverviewDetailsFragment()
            findNavController().navigate(action)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.main_menu, menu)
    }

    @Deprecated("Deprecated in Java")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.main_menu_settings -> {
                val action = MainFragmentDirections.actionMainFragmentToSettingsFragment()
                findNavController().navigate(action)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}