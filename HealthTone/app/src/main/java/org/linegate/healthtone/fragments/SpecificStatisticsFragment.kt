package org.linegate.healthtone.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import org.linegate.healthtone.AddDialog
import org.linegate.healthtone.R
import org.linegate.healthtone.appviewmodel.AppViewModel
import org.linegate.healthtone.databinding.FragmentSpecificStatisticsBinding

class SpecificStatisticsFragment(val type: Type) : Fragment() {

    enum class Type { Water, Eat, Sleep, Steps }

    private var _binding: FragmentSpecificStatisticsBinding? = null
    val binding
        get() = _binding!!

    private val appViewModel: AppViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_main, container, false)
        _binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_specific_statistics, container, false)
        binding.appViewModel = appViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.type = type

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (type) {
            Type.Sleep -> {
                binding.specificStatsAddButton.setOnClickListener {
                    AddDialog(requireContext(), AddDialog.Type.SLEEP, appViewModel).show()
                }
            }
            Type.Water -> {
                binding.specificStatsAddButton.setOnClickListener {
                    AddDialog(requireContext(), AddDialog.Type.WATER, appViewModel).show()
                }
            }
            Type.Eat -> {
                binding.specificStatsAddButton.setOnClickListener {
                    AddDialog(requireContext(), AddDialog.Type.EAT, appViewModel).show()
                }
            }
            Type.Steps -> {
                binding.specificStatsAddButton.visibility = View.GONE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}